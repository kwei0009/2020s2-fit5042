/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.repository;

import fit5042.tutex.repository.entities.Property;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO Exercise 1.3 Step 2 Complete this class.
 * 
 * This class implements the PropertyRepository class. You will need to add the keyword
 * "implements" PropertyRepository. 
 * 
 * @author Kaige Wei
 */
public class SimplePropertyRepositoryImpl implements PropertyRepository {
	private final List<Property> propertyList;

    public SimplePropertyRepositoryImpl() {
        propertyList = new ArrayList<>();
    }

	@Override
	public void addProperty(Property property) throws Exception {
		//Adding the properties only if the properties are neither in the repository nor in the porpertyList.
		if (searchPropertyById(property.getPropertyId()) == null && !propertyList.contains(property)) {
			propertyList.add(property);
		}	
	}

	@Override
	public Property searchPropertyById(int id) throws Exception {
		//iterate all the elements in the ArrayList and get the specific property
		for (Property property : this.propertyList) {
			if (property.getPropertyId() == id) {
				return property;
			}
		}
		return null;
	}

	@Override
	public List<Property> getAllProperties() throws Exception {
		//return the ArrayList which includes all the properties.
		return this.propertyList;
	}
    
}
